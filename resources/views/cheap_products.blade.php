<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    @if($products)
        List of cheap products
        @foreach($products as $product)
            <ul>
                <li>Name: {{ $product->getName() }}</li>
                <li>Price: {{ $product->getPrice() }}</li>
                <li>Image: {{ $product->getImageUrl() }}</li>
                <li>Rating: {{ $product->getRating() }}</li>
            </ul>
        @endforeach
    @else
        <h2>There is no products :(</h2>
    @endif
</body>
</html>
