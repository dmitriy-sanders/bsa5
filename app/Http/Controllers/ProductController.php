<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

class ProductController extends Controller
{
    private GetAllProductsAction $getAllProductsAction;
    private GetMostPopularProductAction $getMostPopularProductAction;
    private GetCheapestProductsAction $getCheapestProductsAction;

    public function __construct(
        GetAllProductsAction $getAllProductsAction,
        GetMostPopularProductAction $getMostPopularProductAction,
        GetCheapestProductsAction $getCheapestProductsAction
    ) {
        $this->getAllProductsAction        = $getAllProductsAction;
        $this->getMostPopularProductAction = $getMostPopularProductAction;
        $this->getCheapestProductsAction   = $getCheapestProductsAction;
    }

    public function getProductsCollection()
    {
        $products = $this->getAllProductsAction->execute()->getProducts();

        $presentedProducts = ProductArrayPresenter::presentCollection($products);

        return response()->json($presentedProducts, 200);
    }

    public function getPopularProduct()
    {
        $popularProduct = $this->getMostPopularProductAction->execute()->getProduct();
        $popularProduct = ProductArrayPresenter::present($popularProduct);

        return response()->json($popularProduct, 200);
    }

    public function getCheapProducts()
    {
        $cheapestProducts = $this->getCheapestProductsAction->execute()->getProducts();
        return view('cheap_products', [
            'products' => $cheapestProducts
        ]);
    }
}
