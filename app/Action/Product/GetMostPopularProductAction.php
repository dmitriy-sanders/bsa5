<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetMostPopularProductAction
{
    public function execute(): GetMostPopularProductResponse
    {
        $productRepository = app('ProductRepository');
        $products = $productRepository->findAll();

        usort($products, function ($prev, $next) {
            return $prev->getRating() < $next->getRating();
        });

        $mostPopularProduct = $products[0];

        return new GetMostPopularProductResponse($mostPopularProduct);
    }
}
