<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetAllProductsAction
{
    public function execute(): GetAllProductsResponse
    {
        $productRepository = app('ProductRepository');
        $products = $productRepository->findAll();

        return new GetAllProductsResponse($products);
    }
}
