<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetCheapestProductsAction
{
    public function execute(): GetCheapestProductsResponse
    {
        $productRepository = app('ProductRepository');
        $products = $productRepository->findAll();

        usort($products, function ($prev, $next) {
            return $prev->getPrice() > $next->getPrice();
        });

        $threeCheapestProducts = array_slice($products, 0, 3);

        return new GetCheapestProductsResponse($threeCheapestProducts);
    }
}
