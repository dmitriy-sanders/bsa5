<?php

namespace App\Providers;

use App\Repository\ProductRepository;
use App\Repository\ProductRepositoryInterface;
use App\Services\ProductGenerator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ProductRepository', function () {
            $prodGenerator = (new ProductGenerator());
            $prods = $prodGenerator->generate();
            return new ProductRepository($prods);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
